import React, {useState} from 'react';
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { Provider as StoreProvider } from 'react-redux';
import reduxStore from './src/redux/store';
import { PaperProvider } from 'react-native-paper';
import { CombinedDarkTheme, CombinedDefaultTheme } from './src/core/theme';
import { PreferencesContext } from './src/core/PreferencesContext';
import { NavigationContainer } from '@react-navigation/native';


const AppWrapper = () => {

  const [isThemeDark, setIsThemeDark] = React.useState(false);

  let theme = isThemeDark ? CombinedDarkTheme : CombinedDefaultTheme;

  const toggleTheme = React.useCallback(() => {
    return setIsThemeDark(!isThemeDark);
  }, [isThemeDark]);

  const preferences = React.useMemo(
    () => ({
      toggleTheme,
      isThemeDark,
    }),
    [toggleTheme, isThemeDark]
  );

  return (
    <StoreProvider store={reduxStore}>
      <PreferencesContext.Provider value={preferences}>
        <PaperProvider theme={theme}>
          <NavigationContainer theme={theme}>
            <App />
          </NavigationContainer>
        </PaperProvider>
      </PreferencesContext.Provider>
    </StoreProvider>
  );
}
  


AppRegistry.registerComponent(appName, () => AppWrapper);
