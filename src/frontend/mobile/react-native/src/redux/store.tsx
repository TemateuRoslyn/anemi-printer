import { createStore, applyMiddleware, combineReducers } from "redux";
//reducers
import authReducer from "./reducers/authReducer";

const rootReducer = combineReducers({
  auth: authReducer,
});

const reduxStore = createStore(rootReducer);

export default reduxStore;
