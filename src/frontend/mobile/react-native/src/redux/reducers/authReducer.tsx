// import AsyncStorage from "@react-native-async-storage/async-storage";
import { LOGIN_SUCCESS, LOGOUT } from "../actions/type";
import { ReduxActionType, User } from "../../models/models";

// Récupérer l'utilisateur de manière asynchrone
// const getUserFromStorage = async (): Promise<User | null> => {
//     const userString: string | null = await AsyncStorage.getItem(AUTH_USER_ASC);
//     if (userString) {
//       const user: User = JSON.parse(userString);
//       return user;
//     } else {
//       return null;
//     }
// };
// Définir l'interface de l'état d'authentification
interface AuthState {
    isLoggedIn: boolean;
    user: User | null;
};

// Initialiser l'état avec la valeur récupérée de AsyncStorage
// const initializeState = async (): Promise<AuthState> => {
//     const user: User | null = await getUserFromStorage();
//     return {
//       isLoggedIn: user !== null,
//       user: user
//     };
// };

// Créer l'état initial en utilisant une fonction asynchrone
// const initialState: AuthState = await initializeState();

// Créer le reducer
const authReducer = (state: AuthState = {
    isLoggedIn: false,
    user: {username: 'baseLogin', password: 'basePassword'}
}, action: ReduxActionType): AuthState => {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                isLoggedIn: true,
                user: action.payload.user,
            };
        case LOGOUT:
            return {
                ...state,
                isLoggedIn: false,
                user: null,
            };
        default:
            return state;
    }
};

export default authReducer;
