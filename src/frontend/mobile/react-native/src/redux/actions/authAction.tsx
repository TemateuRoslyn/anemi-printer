import { Dispatch } from "redux";
import { AuthResponse, ReduxActionType, User } from "../../models/models";
import authService from "../../services/authService";

import { LOGIN_FAIELD, LOGIN_SUCCESS, LOGOUT } from "./type";

/**
 * Action de connexion d'un utilisateur.
 * @param user Les informations de l'utilisateur à connecter.
 * @returns Une fonction qui prend en paramètre un dispatch et retourne une promesse résolue avec la réponse de la connexion si réussie, sinon une erreur.
 */
export const loginAction = (user: User): ReduxActionType => {
  if(user && user.isAuthenticate === true){
    return { 
      type: LOGIN_SUCCESS, 
      payload: user 
    }
  } else{
    return { 
      type: LOGIN_FAIELD, 
      payload: null
    }
  }
};

/**
 * Action de déconnexion de l'utilisateur.
 * @returns Une promesse résolue avec la réponse de la déconnexion si réussie, sinon une erreur.
 */
// export const logoutAction = () => (dispatch: Dispatch): Promise<AuthResponse> => {
//     return authService.logOut()
//         .then((response: AuthResponse) => {
//             if (response.status === "success") {
//                 dispatch({
//                     type: LOGOUT,
//                 });
//                 return Promise.resolve(response);
//             } else {
//                 return Promise.reject(new Error(response.message));
//             }
//         });
// };
