import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { ForgotPasswordScreen, LoginScreen, RegisterScreen, SplashScreen } from "../../screens/auth";


const AuthStack = createNativeStackNavigator();

const AuthStackNavigator = () => (
  <AuthStack.Navigator initialRouteName="DashboardScreen">
    <AuthStack.Screen
      name="SplashScreen"
      component={SplashScreen}
      options={{ headerShown: false }}
    />
    <AuthStack.Screen
      name="LoginScreen"
      component={LoginScreen}
      options={{ headerShown: false }}
    />
    <AuthStack.Screen
      name="RegisterScreen"
      component={RegisterScreen}
      options={{ headerShown: false }}
    />
    <AuthStack.Screen
      name="ForgotPasswordScreen"
      component={ForgotPasswordScreen}
      options={{ headerShown: false }}
    />
  </AuthStack.Navigator>
);

export default AuthStackNavigator;
