import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { DashboardScreen, HomeScreen } from "../../screens/app";

const AppStack = createNativeStackNavigator();


const AppStackNavigator = () => (
  <AppStack.Navigator initialRouteName="DashboardScreen">
     <AppStack.Screen
      name="DashboardScreen"
      component={DashboardScreen}
      options={{ headerShown: false }}
    />
    <AppStack.Screen
      name="HomeScreen"
      component={HomeScreen}
      options={{ headerShown: false }}
    />
  </AppStack.Navigator>
);

export default AppStackNavigator;
