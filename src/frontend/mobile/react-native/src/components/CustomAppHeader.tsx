import * as React from 'react';
import { Platform } from 'react-native';
import { Appbar, Badge, useTheme } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';



type Props = {
  _handlePrint?: () => void;
  _handleUpload?: () => void;
};

const CustomAppHeader = ({ _handlePrint,  _handleUpload}: Props) => {

  const theme = useTheme();

  const MORE_ICON = Platform.OS === 'ios' ? 'dots-horizontal' : 'dots-vertical';

  return (
    <Appbar.Header>
      <Appbar.Content title="Anemi AI" color={theme.colors.onSurface} titleStyle={{fontWeight: 'bold'}} />
     {_handlePrint && <Appbar.Action icon={() => <MaterialCommunityIcons name="printer" color={'#21005D'} size={26} />} onPress={_handlePrint} />} 
     {_handleUpload && <Appbar.Action icon={() => <MaterialCommunityIcons name="file-upload" color={'#21005D'} size={26} />} onPress={_handleUpload} />}
     <Appbar.Action icon={MORE_ICON} onPress={() => {}} />
  
    </Appbar.Header>
  );
};

export default CustomAppHeader;