import React, { memo, useState } from 'react';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';

import { emailValidator, passwordValidator } from '../../../core/utils';
import { Navigation } from '../../../navigation/types';

import Background from '../../../components/Background';
import Logo from '../../../components/Logo';
import Header from '../../../components/Header';
import Button from '../../../components/Button';
import BackButton from '../../../components/BackButton';
import TextInput from '../../../components/TextInput';

import { AuthResponse, User } from '../../../models/models';

import authService from '../../../services/authService';

import { useDispatch, useSelector } from 'react-redux';
import { Dispatch } from 'redux';
import { loginAction } from '../../../redux/actions/authAction';
import { useTheme } from 'react-native-paper';

// import AsyncStorage from '@react-native-async-storage/async-storage';
// import { AUTH_USER_ASC } from '../../../utils/constants';

type Props = {
  navigation: Navigation;
};

const LoginScreen = ({ navigation }: Props) => {
  const [email, setEmail] = useState({ value: '', error: '' });
  const [password, setPassword] = useState({ value: '', error: '' });
  const dispatch: Dispatch<any> = useDispatch();
  const theme = useTheme();

  const _onLoginPressed = async () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);


    if (emailError || passwordError) {
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    }

    const user: User = {
      username: email.value,
      password: password.value,
    };

    // const userString: string | null = await AsyncStorage.getItem(AUTH_USER_ASC);
    // if (userString) {
    //     const user: User = JSON.parse(userString);
    //     if(user.isAuthenticate === true){
    //       console.log("Login successful");
    //     }else {
    //       console.error("Login failed LoginScreen");
    //     }
    // } else {
    //       console.error("Login failed LoginScreen");
    // }

    const response: AuthResponse = authService.logIn(user)
    user.isAuthenticate = response.isAuthenticate


    // authentification de l'utilisateur
    dispatch(loginAction(user));    
  };

  return (
    <Background>
      <BackButton goBack={() => navigation.navigate('SplashScreen')} />

      <Logo />

      <Header>Welcome back.</Header>

      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={(text: string) => setEmail({ value: text, error: '' })}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoComplete="email"
        textContentType="emailAddress"
        keyboardType="email-address"
      />

      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={text => setPassword({ value: text, error: '' })}
        error={!!password.error}
        errorText={password.error}
        secureTextEntry
      />

      <View style={styles.forgotPassword}>
        <TouchableOpacity
          onPress={() => navigation.navigate('ForgotPasswordScreen')}
        >
          <Text style={[styles.label, {color: theme.colors.secondary}]}>Forgot your password?</Text>
        </TouchableOpacity>
      </View>

      <Button mode="contained" onPress={_onLoginPressed}>
        Login
      </Button>

      <View style={styles.row}>
        <Text style={[styles.label, {color: theme.colors.secondary}]}>Don’t have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen')}>
          <Text style={[styles.link, {color: theme.colors.primary}]}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  label: {
    // color: 'gray',
  },
  link: {
    fontWeight: 'bold',
    // color: 'blue',
  },
});

export default memo(LoginScreen);
