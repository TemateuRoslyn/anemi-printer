import React, { memo } from 'react';
import { Navigation } from '../../../navigation/types';
// components
import Background from '../../../components/Background';
import Logo from '../../../components/Logo';
import Header from '../../../components/Header';
import Button from '../../../components/Button';
import Paragraph from '../../../components/Paragraph';

type Props = {
  navigation: Navigation;
};

const SplashScreen = ({ navigation }: Props) => (
  <Background>
    <Logo />
    <Header>Anemi printer 4.0</Header>

    <Paragraph>
      The easiest way to start handle PDF files.
    </Paragraph>
    <Button mode="contained" onPress={() => navigation.navigate('LoginScreen')}>
      Login
    </Button>
    <Button
      mode="outlined"
      onPress={() => navigation.navigate('RegisterScreen')}
    >
      Sign Up
    </Button>
  </Background>
);

export default memo(SplashScreen);
