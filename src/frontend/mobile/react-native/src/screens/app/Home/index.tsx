import React, { useCallback, useState } from 'react';
import {
  Dimensions,
  Platform,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Pdf from 'react-native-pdf';
import DocumentPicker, {
  DirectoryPickerResponse,
  DocumentPickerResponse,
} from 'react-native-document-picker';
import { useEffect } from 'react';

import { Avatar, Text } from 'react-native-paper';
import RNPrint from 'react-native-print';


import CustomAppHeader from '../../../components/CustomAppHeader';
import Logo from '../../../components/Logo';

function HomeScreen(): React.JSX.Element {

  
  const [pdfSource, setPdfSource] = useState<{ uri: any; cache: boolean; }>();
  
  const [result] = React.useState<Array<DocumentPickerResponse> | DirectoryPickerResponse | undefined | null>();
  const [visible, setVisible] = React.useState(true);
  
  useEffect(() => {
    console.log(JSON.stringify(result, null, 2))
  }, [result])

  const _handlePrint = async () => {
    try {
      const printerUrl = Platform.OS === 'ios' ? pdfSource?.uri : `${pdfSource?.uri}`;
      await RNPrint.print({ filePath: printerUrl });
    } catch (err) {
      console.error('Erreur lors de l\'impression:', err);
    }
  }

  const _handleUpload = () => handleDocumentSelection();

  const handleDocumentSelection = useCallback(async () => {
    try {
      const res: DocumentPickerResponse[] = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });
      console.log(res);
      
      const source = { uri: res[0].uri, cache: true };
      setPdfSource(source); // Ajoutez cela à votre état
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // L'utilisateur a annulé la sélection
      } else {
        console.error('Erreur inattendue:', err);
      }
    }
  }, []);
  return (
    <>
      {pdfSource ? <CustomAppHeader _handlePrint={_handlePrint} _handleUpload={_handleUpload} />
                 : <CustomAppHeader/>}
      <SafeAreaView style={styles.container} >
          {!pdfSource && (
            <TouchableOpacity onPress={handleDocumentSelection}>
              <Logo />
              <Text variant="headlineSmall" style={styles.pdfText}>Select a PDF file.</Text>
            </TouchableOpacity>
          )}
          {pdfSource && (
            <Pdf
              trustAllCerts={false}
              source={pdfSource}
              onLoadComplete={(numberOfPages, filePath) => {
                console.log(`Nombre de pages: ${numberOfPages}`);
              }}
              onPageChanged={(page, numberOfPages) => {
                console.log(`Page actuelle: ${page}`);
              }}
              onError={(error) => {
                console.log(error);
              }}
              onPressLink={(uri) => {
                console.log(`Lien pressé: ${uri}`);
              }}
              style={styles.pdf}
            />
          )}
      
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pdf: {
      flex:1,
      width:Dimensions.get('window').width,
      height:Dimensions.get('window').height,
  },
  pdfText: {
    textAlign: 'center',
    color: '#21005D'
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
  
});

export default HomeScreen;
