import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
} from 'react-native';
import { Avatar, Banner } from 'react-native-paper';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import CustomAppHeader from '../../../components/CustomAppHeader';
import LogoSecond from '../../../components/LogoSecond';

function AbousUsScreen(): React.JSX.Element {
  const [visible, setVisible] = React.useState(true);

  return (
    <>
      <CustomAppHeader/>
      <Banner
          visible={visible}
          actions={[
            {
              label: 'Hide',
              onPress: () => setVisible(false),
            },
            
          ]}
          icon={({size}) => (
            <FontAwesome name="file-pdf-o" color={'black'} size={size} />
          )}>
          Unlock the power of knowledge with our PDF library. Welcome aboard!
        </Banner>
      <SafeAreaView style={styles.container} >
        <LogoSecond/>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center'
  },
});

export default AbousUsScreen;
