import React, { memo } from 'react';
import { Navigation } from '../../../navigation/types';
// components
import Background from '../../../components/Background';
import Logo from '../../../components/Logo';
import Header from '../../../components/Header';
import Paragraph from '../../../components/Paragraph';
import Button from '../../../components/Button';

type Props = {
  navigation: Navigation;
};

const DashboardScreen = ({ navigation }: Props) => (
  <Background>
    <Logo />
    <Header>Let’s start</Header>
    <Paragraph>
      Your amazing app starts here. Open you favourite code editor and start
      editing this project.
    </Paragraph>
    <Button mode="outlined" onPress={() => navigation.navigate('HomeScreen')}>
      Logout
    </Button>
  </Background>
);

export default memo(DashboardScreen);
