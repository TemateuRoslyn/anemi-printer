import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
} from 'react-native';
import CustomAppHeader from '../../../components/CustomAppHeader';
import { Avatar, Chip, List, Switch, Text, useTheme } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { PreferencesContext } from '../../../core/PreferencesContext';



function SettingsScreen(): React.JSX.Element {

  const theme = useTheme();
  const { toggleTheme, isThemeDark } = React.useContext(PreferencesContext);
  


  return (
    <>
      <CustomAppHeader/>
      <SafeAreaView style={[styles.container]} >
          
          <View style={[styles.profileBlock, styles.profileBlock1]}>
            <Avatar.Image size={100} source={require('./../../../assets/avatars/maestros-avatar.png')} />
            <Text variant="headlineSmall" style={[styles.profileText, styles.profileNameText]}>Roslyn TEMATEU</Text>
            <Text variant="titleLarge">maestros2.0</Text>
            <Chip icon={() => <MaterialCommunityIcons name="account-edit-outline" color={theme.colors.onSurface} size={26} />} mode="outlined" selectedColor="black" onPress={() => console.log('Pressed')}><Text>Edit Profile</Text></Chip>
          </View>

          <View style={[styles.profileBlock2]}>
            <List.Item
              titleStyle={{color: 'black'}}
              title={!isThemeDark ? 'Darck Mode' : 'Light Mode'}
              left={props => !isThemeDark ? <Ionicons {...props} name={'moon'} size={26} /> : <Ionicons {...props} name={'sunny-outline'} size={26} color={'black'}  />}
              right={props => <Switch value={isThemeDark} onValueChange={toggleTheme} color={!isThemeDark ? '#A03EA9' : 'black'} />}
            />
            
          </View>

      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
  },
  profileText: {
    // color: 'black',
  },
  profileNameText: {
    textAlign: 'center',
    fontWeight: 'bold',
  },
  profileUserNameText:{
    color: 'gray',
  },
  profileBlock: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  profileBlock1: {
    flex: 1,
    width: '100%',
    // backgroundColor: 'red',
  },
  profileBlock2: {
    flex: 2,
    backgroundColor: '#FFFFFF',
    width: '100%',
  }
});


export default SettingsScreen;
