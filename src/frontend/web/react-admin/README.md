# Anemi Printer Dashboard - React Application

## Description

Il s'agit de l'application web ou les administrateur monitore Animi printer.

## Démarrage

Ce projet utilise Vite pour le développement et la construction ainsi que Tailwind CSS pour le design des interfaces.

### Prérequis

* node@18.19.0 installés sur votre système.
* npm@10.2.3 installés sur votre système.

### Installation

1. Accédez au répertoire du projet.

```sh
cd src/frontend/web/react-admin 
```

2. Installez les dépendances :

```sh
npm install
```

4. Démarrer le projet

```sh
npm run dev
```

5. Visualiser l'application dashboard sur le port indique dansla console.