import { Route, Routes } from "react-router-dom";
import Landing from "./views/Landing";

/**
 * 
 * @returns 
 */
function App() {
  return (
    <Routes>
        <Route
          index
          element={<Landing/>}
        />
        <Route
          path="/"
          element={<Landing/>}
        />
        <Route
          path="/landing"
          element={<Landing/>}
        />
    </Routes>
  );
}

export default App;
