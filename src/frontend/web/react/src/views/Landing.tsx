import { 
  Footer,
  Navbar,
  
} from "../components";


export default function Landing() {
  return (
    <>
      <div>
        <Navbar />
        <Footer />
      </div>
    </>
  );
}
