import React from "react";



const Navbar: React.FC<{}> = () => {
  return (
    <nav className="fixed z-50 w-full bg-white top-0 flex flex-wrap items-center justify-between px-2 py-3 shadow-lg">
        <div className="container px-4 mx-auto flex flex-wrap items-center justify-between">
            <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
                <a className="text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase text-blueGray-700" href="#">Anemi Printer 4.0</a>
                <button className="cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none" type="button">
                    <i className="fas fa-bars"></i>
                </button>
            </div>
            <div className="lg:flex flex-grow items-center hidden" id="example-navbar-danger">
                <ul className="flex flex-col lg:flex-row list-none lg:ml-auto">
                    <li className="nav-item">
                        <a
                            href="#"
                            className="px-3 py-2 flex items-center text-xs uppercase font-bold text-blueGray-700 hover:text-blueGray-500 border border-solid border-gray-500 rounded-lg"
                            target="_blank"
                        >
                            <i className="fab fa-sketch text-lg leading-lg text-blueGray-400"></i><span className="ml-2">Pro Version</span><span className="bg-red-500 text-white ml-2 px-2 rounded-full">NEW</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="download-button px-3 py-2 flex items-center text-xs uppercase font-bold text-blueGray-700 hover:text-blueGray-500" href="#">
                            <i className="fas fa-arrow-alt-circle-down text-lg leading-lg text-blueGray-400"></i><span className="ml-2">Download</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="px-3 py-2 flex items-center text-xs uppercase font-bold text-blueGray-700 hover:text-blueGray-500" href="#">
                            <i className="far fa-file-alt text-lg leading-lg text-blueGray-400"></i><span className="ml-2">Docs</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a
                            href="#"
                            className="px-3 py-2 flex items-center text-xs uppercase font-bold text-blueGray-700 hover:text-blueGray-500"
                            target="_blank"
                        >
                            <i className="fab fa-facebook-square text-lg leading-lg text-blueGray-400"></i>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a
                            href="#"
                            className="px-3 py-2 flex items-center text-xs uppercase font-bold text-blueGray-700 hover:text-blueGray-500"
                            target="_blank"
                        >
                            <i className="fab fa-twitter text-lg leading-lg text-blueGray-400"></i>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="px-3 py-2 flex items-center text-xs uppercase font-bold text-blueGray-700 hover:text-blueGray-500" target="_blank">
                            <i className="fab fa-github text-lg leading-lg text-blueGray-400"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
  );
};

export default Navbar;