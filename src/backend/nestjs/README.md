# Anemi Printer Backend - Nest.js Application

## Description

Ce repertoire contien le code de l'applicatoin backend principale.

## Démarrage

Ce projet utilise nest pour le développement et la construction des END points.

### Prérequis

* node@18.19.0
* npm@10.2.3


### Installation & Démarrage

1. Accédez au répertoire du projet.

```sh
cd src/backend/nestjs
```

2. Installez les dépendances :

```sh
npm install
```

4. Démarrer le serveur JS

```sh
npm run start
```

5. Le serveur indiquera le port sur lequel il est disponible dans la console de demarage..